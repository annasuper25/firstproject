package ru.digitalleague.checkratesapi.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.digitalleague.checkratesapi.client.openExchangeRates.ExchangeRatesClient;
import ru.digitalleague.checkratesapi.client.openExchangeRates.dto.ExchangeRatesResponse;
import ru.digitalleague.checkratesapi.configuration.CheckRatesProperties;

@RequiredArgsConstructor
@Service
public class ExchangeRateService {

    private final CheckRatesProperties properties;
    private final ExchangeRatesClient exchangeRatesClient;

    public Double getRate() {
        ExchangeRatesResponse ratesToday = exchangeRatesClient.getLastRates(properties.getRateAppId());
        if (isRateExist(ratesToday)) {
            return ratesToday.getRates().get(properties.getCurrency());
        }

        return null;
    }

    private boolean isRateExist(ExchangeRatesResponse ratesToday) {
        return ratesToday != null && ratesToday.getRates() != null
                && ratesToday.getRates().get(properties.getCurrency()) != null;
    }

}
