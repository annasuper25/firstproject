package ru.digitalleague.humanapi;

import java.time.LocalDate;
import java.util.List;
import java.util.TreeMap;

import static ru.digitalleague.humanapi.Gender.MALE;

public class Main {
    public static void main(String[] args) {

        WorkWithHuman work = new WorkWithHuman();
        List<Human> list = work.createList();
        Human petr = new Human("Петр", "Абрамов", "Дмитриевич", 17, MALE, LocalDate.of(2004, 1, 14));

        System.out.println(work.isHumanInList(petr, list));
        System.out.println();

        List<Human> newList = work.searchYoungPeople(list);
        newList.forEach(System.out::println);
        System.out.println();

        TreeMap<Human, Long> map2 = work.searchDuplicatePeople(list);
        map2.forEach((k, v) -> System.out.println(v + " : [" + k + "]"));
    }
}
