package ru.digitalleague.humanapi;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Human implements Comparable<Human> {
    private String name;
    private String surname;
    private String patronymic;
    private Integer age;
    private Gender gender;
    private LocalDate birthDate;

    @Override
    public int compareTo(Human h) {
        return this.getAge().compareTo(h.getAge());
    }
}
