package ru.digitalleague.checkratesapi.configuration;


import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import javax.validation.constraints.NotEmpty;

@Getter
@AllArgsConstructor
@ConstructorBinding
@ConfigurationProperties(prefix = "main")
public class CheckRatesProperties {

    @NotEmpty
    private String rateAppId;

    @NotEmpty
    private String currency;

    @NotEmpty
    private String openRateEndpoint;
}
