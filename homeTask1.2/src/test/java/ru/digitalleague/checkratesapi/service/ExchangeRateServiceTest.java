package ru.digitalleague.checkratesapi.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.digitalleague.checkratesapi.client.openExchangeRates.ExchangeRatesClient;
import ru.digitalleague.checkratesapi.client.openExchangeRates.dto.ExchangeRatesResponse;
import ru.digitalleague.checkratesapi.configuration.CheckRatesProperties;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;

@SpringBootTest
public class ExchangeRateServiceTest {

    @Autowired
    ExchangeRateService exchangeRateService;

    @Autowired
    CheckRatesProperties properties;

    @MockBean
    ExchangeRatesClient exchangeRatesClient;

    @Test
    public void shouldReturnRateWhenSuccessfulResponse() {
        when(exchangeRatesClient.getLastRates(properties.getRateAppId())).thenReturn(createExchangeRatesResponse(74.5));

        Double rate = exchangeRateService.getRate();

        Assertions.assertEquals(74.5, rate);
    }

    @Test
    public void shouldReturnNullWhenRateDoestNotExist() {
        when(exchangeRatesClient.getLastRates(properties.getRateAppId())).thenReturn(
                ExchangeRatesResponse.builder()
                        .rates(new HashMap<>())
                        .build());

        Double rate = exchangeRateService.getRate();

        Assertions.assertNull(rate);
    }

    private ExchangeRatesResponse createExchangeRatesResponse(Double rate) {
        Map<String, Double> currency = new HashMap<>();
        currency.put(properties.getCurrency(), rate);

        return ExchangeRatesResponse.builder()
                .rates(currency)
                .build();
    }
}
