package ru.digitalleague.humanapi;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static ru.digitalleague.humanapi.Gender.FEMALE;
import static ru.digitalleague.humanapi.Gender.MALE;

public class WorkWithHuman implements SearchHumans {

    public List<Human> createList(){
        Human lisa = new Human("Лиза", "Иванова", "Ивановна", 18, FEMALE, LocalDate.of(2002, 04, 21));
        Human maks = new Human("Максим", "Бутаков", "Валерьевич", 28, MALE, LocalDate.of(1992, 9, 26));
        Human petr = new Human("Петр", "Абрамов", "Дмитриевич", 17, MALE, LocalDate.of(2004, 01, 14));
        Human masha = new Human("Мария", "васильева", "Александровна", 16, FEMALE, LocalDate.of(2005, 02, 9));

        List<Human> humanList = new ArrayList<Human>();

        humanList.add(lisa);
        humanList.add(maks);
        humanList.add(petr);
        humanList.add(masha);
        humanList.add(masha);
        humanList.add(lisa);
        humanList.add(lisa);

        return humanList;
    }

    public List<Human> searchYoungPeople(List<Human> humanList) {

        List<Human> newList = humanList.stream()
                .filter((p) -> p.getAge() <= 20
                        && (p.getSurname().toLowerCase().startsWith("а")
                        || p.getSurname().toLowerCase().startsWith("б")
                        || p.getSurname().toLowerCase().startsWith("в")
                        || p.getSurname().toLowerCase().startsWith("г")
                        || p.getSurname().toLowerCase().startsWith("д")))
                .distinct()
                .collect(Collectors.toList());

        return newList;
    }

    public boolean isHumanInList(Human human, List<Human> humanList) {

        return humanList.stream()
                .anyMatch(human::equals);
    }

    public TreeMap<Human, Long> searchDuplicatePeople(List<Human> humanList) {

        Map<Human, Long> map = humanList.stream()
                .collect(Collectors.groupingBy(c -> c, Collectors.counting()));

        TreeMap<Human, Long> map2 = new TreeMap<>(map);
        return map2;


    }
}
