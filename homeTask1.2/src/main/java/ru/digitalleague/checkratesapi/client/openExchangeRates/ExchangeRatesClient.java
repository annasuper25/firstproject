package ru.digitalleague.checkratesapi.client.openExchangeRates;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import ru.digitalleague.checkratesapi.client.openExchangeRates.dto.ExchangeRatesResponse;


@FeignClient(
        name = "open-exchange-rates-client",
        url = "https://openexchangerates.org/api"
)
public interface ExchangeRatesClient {

    @GetMapping("/latest.json")
    ExchangeRatesResponse getLastRates(@RequestParam(value = "app_id") String appId);

}
