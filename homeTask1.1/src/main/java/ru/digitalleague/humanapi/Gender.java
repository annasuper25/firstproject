package ru.digitalleague.humanapi;

public enum Gender {
    MALE,
    FEMALE
}
