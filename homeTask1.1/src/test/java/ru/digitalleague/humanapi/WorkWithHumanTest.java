package ru.digitalleague.humanapi;

import org.junit.Test;

import java.time.LocalDate;
import java.util.List;
import java.util.TreeMap;

import static org.junit.Assert.*;
import static ru.digitalleague.humanapi.Gender.MALE;


public class WorkWithHumanTest {

    private final WorkWithHuman worker = new WorkWithHuman();
    private final List<Human> humanList = worker.createList();
    Human petr = new Human("Петр", "Абрамов", "Дмитриевич", 17, MALE, LocalDate.of(2004, 01, 14));

    @Test
    public void shouldReturnTrueIfHumanIsInList() {
        boolean actual = worker.isHumanInList(petr, humanList);

        assertEquals(true, actual);
    }

    @Test
    public void shouldReturnPeopleYounger20FromList() {
        List<Human> actualList = worker.searchYoungPeople(humanList);

        assertEquals(2, actualList.size());
    }

    @Test
    public void shouldReturnDuplicatePeopleInCorrectOrder() {
        TreeMap<Human, Long> actualHumanMap = worker.searchDuplicatePeople(humanList);

        assertEquals(4, actualHumanMap.size());
        assertEquals(16, (int) actualHumanMap.firstEntry().getKey().getAge());
        assertEquals(2, (long) actualHumanMap.firstEntry().getValue());
        assertEquals(28, (int) actualHumanMap.lastEntry().getKey().getAge());
        assertEquals(1, (long) actualHumanMap.lastEntry().getValue());
    }
}