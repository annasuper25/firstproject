package ru.digitalleague.humanapi;

import java.util.List;
import java.util.TreeMap;

public interface SearchHumans {

    List<Human> searchYoungPeople(List<Human> humanList);

    boolean isHumanInList (Human human, List<Human> humanList);

    TreeMap<Human, Long> searchDuplicatePeople(List<Human> humanList);
}
