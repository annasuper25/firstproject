package ru.digitalleague.checkratesapi.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.digitalleague.checkratesapi.service.ExchangeRateService;

@RestController
@RequiredArgsConstructor
public class CheckRateController {

    static final String URL = "/rate";

    private final ExchangeRateService exchangeRateService;

    @GetMapping(URL)
    public Double getRate() {
        return exchangeRateService.getRate();
    }

}
